package org.hometask.models;

import java.util.HashMap;

public class TestData {

    public static HashMap<String , String > getDataForCourses(){
        HashMap<String,String> cources = new HashMap<>();
        cources.put("Введение в алгоритмы и структуры данных","1 семестр");
        cources.put("Основы линейной алгебры","1 семестр");
        cources.put("Статистика для начинающих","1 семестр");
        cources.put("Программирование на Python","2 семестра");
        cources.put("Веб-разработка с использованием HTML, CSS и JavaScript","2 семестра");
        cources.put("Математический анализ","3 семестра");
        cources.put("Дифференциальные уравнения","1 семестр");
        cources.put("Теория вероятностей и математическая статистика","3 семестра");
        cources.put("Базы данных и SQL","1 семестр");
        cources.put("Объектно-ориентированное программирование","2 семестра");
        cources.put("Компьютерная графика","1 семестр");
        cources.put("Искусственный интеллект и машинное обучение","2 семестра");
        cources.put("Операционные системы","1 семестр");
        cources.put("Электротехника","2 семестра");
        cources.put("Электроника","2 семестра");
        cources.put("Квантовая механика","2 семестра");
        cources.put("Классическая механика","2 семестра");
        cources.put("Астрономия","1 семестр");
        cources.put("Экономика","2 семестра");
        cources.put("Финансы","1 семестр");
        cources.put("Маркетинг","1 семестр");
        cources.put("Менеджмент","1 семестр");
        cources.put("Психология","1 семестр");
        cources.put("Социология","1 семестр");
        cources.put("Философия","1 семестр");
        cources.put("История","1 семестр");
        cources.put("Литература","1 семестр");
        cources.put("Искусствоведение","1 семестр");
        cources.put("Экология","1 семестр");
        cources.put("Биология","1 семестр");
        cources.put("Химия","1 семестр");
        return cources;
    }


}
