package SeminarTask_02.Thing;

import SeminarTask_02.Interfaces.Thing;

/**
 * Блокнот
 */
public class Notebook implements Thing {
    @Override
    public String getName() {
        return "Блокнот";
    }
}
