package SeminarTask_02.Thing;

import SeminarTask_02.Interfaces.Thing;

/**
 * Ручка
 */
public class Pen implements Thing {
    @Override
    public String getName() {
        return "Ручка";
    }
}
