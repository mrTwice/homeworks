package SeminarTask_02.Interfaces;

/**
 * Вещь
 */
public interface Thing {

    /**
     * Получить наименование вещи
     * @return наименование вещи
     */
    String getName();

}

