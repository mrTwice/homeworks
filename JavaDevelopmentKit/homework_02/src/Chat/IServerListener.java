package Chat;

public interface IServerListener {
    void receiveMessage(String message);
}
