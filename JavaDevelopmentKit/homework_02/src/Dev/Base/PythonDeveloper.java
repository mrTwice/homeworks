package Dev.Base;

import Dev.Interfaces.FrontendDeveloper;

public class PythonDeveloper implements FrontendDeveloper {
    @Override
    public void writeUICode() {
        System.out.println("PythonDeveloper writes python UIs code...");
    }
}
