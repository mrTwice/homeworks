package Dev.Interfaces;

import Dev.Interfaces.BackendDeveloper;
import Dev.Interfaces.FrontendDeveloper;

public interface FullStackDeveloper extends BackendDeveloper, FrontendDeveloper {
}
