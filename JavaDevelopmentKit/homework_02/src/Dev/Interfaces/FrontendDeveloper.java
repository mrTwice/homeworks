package Dev.Interfaces;

public interface FrontendDeveloper extends Developer {
    void writeUICode();
}
