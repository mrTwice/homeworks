package Dev.Interfaces;

public interface ProjectFactory {
    FullStackDeveloper getFullstackDeveloper();
    BackendDeveloper getBackendDeveloper();
    FrontendDeveloper getFrontendDeveloper();
}
