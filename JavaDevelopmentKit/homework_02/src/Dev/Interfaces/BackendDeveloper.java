package Dev.Interfaces;

public interface BackendDeveloper extends Developer {
    void writeServersCode();
}
