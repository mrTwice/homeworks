package Task_02;

import Task_02.MVP.Presenter;

public class Main {
    public static void main(String[] args) {
        Presenter calculator = new Presenter();
        calculator.start();
    }
}