package ru.yampolskiy.taskclient.models.task;

public enum TaskStatus {
    OPEN,
    IN_PROGRESS,
    COMPLETED
}
