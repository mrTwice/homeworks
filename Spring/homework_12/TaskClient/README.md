# Сохранение информации в файл с помощью Spring Integration

Код по данному домашнему заданию добавлен в:

* [FileGateway](https://github.com/mrTwice/GB_HomeWork/blob/main/Spring/homework_12/TaskClient/src/main/java/ru/yampolskiy/taskclient/service/FileGateway.java)

* [TaskManagerController](https://github.com/mrTwice/GB_HomeWork/blob/main/Spring/homework_12/TaskClient/src/main/java/ru/yampolskiy/taskclient/controller/TaskManagerController.java)

* [IntegrationConfig](https://github.com/mrTwice/GB_HomeWork/blob/main/Spring/homework_12/TaskClient/src/main/java/ru/yampolskiy/taskclient/config/IntegrationConfig.java)

