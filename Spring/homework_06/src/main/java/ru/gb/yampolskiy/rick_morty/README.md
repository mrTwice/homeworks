### Домашнее задание по семинару № 6

Для проверки, после запуска проекта открыть [открыть ссылку](http://localhost:8080/characters/)


* [RickMortyController.java](controller%2FRickMortyController.java) контроллер обработки запросов и представлений


* [ObjectService.java](service%2FObjectService.java) является абстрактным и содержит в себе ссылку на API и обобщенный метод, для получения информации для наполнения базовых доменных моделей.


* [CharacterService.java](service%2FCharacterService.java) сервисный клас отвечающий за получения данных о персонажах


* [EpisodeService.java](service%2FEpisodeService.java) сервисный клас отвечающий за получения данных об эпизодах


* [LocationService.java](service%2FLocationService.java) сервисный клас отвечающий за получения данных о локациях