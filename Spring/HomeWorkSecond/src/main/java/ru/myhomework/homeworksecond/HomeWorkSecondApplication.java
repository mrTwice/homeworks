package ru.myhomework.homeworksecond;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeWorkSecondApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeWorkSecondApplication.class, args);
	}

}
