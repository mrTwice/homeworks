package ru.yampolskiy.trackuseraction.model.annotation;

public enum ProductActions {
    CREATE,
    READ,
    UPDATE,
    DELETE

}
