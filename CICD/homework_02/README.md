# Домашнее задание № 2

1. Написать Dockerfile для создания образа
2. Создать pipeline для запуска контейнера из созданного образа, который бы проверял, что контейнер стартует
3. Поэкспериментировать с этим...

## Что проделано

1. Напислал простой, чтобы было что запустить [Dockerfile](https://gitlab.com/mrTwice/homeworks/-/blob/main/Dockerfile?ref_type=heads)
2. Сконфигурировал [pipeline](https://gitlab.com/mrTwice/homeworks/-/blob/main/.gitlab-ci.yml?ref_type=heads)
3. Настройка переменных окружения:
  * В Settings > Repository > Deploy Tokens созда токен
  * В Settings > CI/CD > Variables создать переменные окружения со значениями полученными в предыдущем пункте
4. Добавил cache в pipeline, само кеширования не реализовывал ввиду отсутствия проекта на java
5. Добавил этам docker_image_build, в котором собирается обрах из [Dockerfile](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_02/Dockerfile?ref_type=heads)
6. Добавил этап docker_image_build_test, который проверяет был ли собран образ после послежнего коммита, и загружен ли он в реестр 
