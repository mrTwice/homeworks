# Домашнее задание №3

1. Добавить 2 окружения "preprod" и "production".

2. Добавить отдельные deploy job для каждой среды.

3. Добавить переменную "$MyLogin" внутри .gitlab-ci.yml, которая будет меняться в зависимости от среды.

3. Добавить переменную "$MyPassword" не используя .gitlab-ci.yml, которая так же будет меняться в зависимости от среды.

4. *Добавить скрипт в .gitlab-ci.yml, который найдёт все запущенные pipeline по названии ветки(ref) и остановит их.

# Проделано:

1. Пайплайн разбит нанесколько отдельных [файлов](https://gitlab.com/mrTwice/homeworks/-/tree/main/CICD/homework_03/pipeline?ref_type=heads), отвечающих за отдельные стейджы
2. В основной [.gitlab-ci.yml ](https://gitlab.com/mrTwice/homeworks/-/blob/main/.gitlab-ci.yml?ref_type=heads) добавленна ссылка на [pipeline.yml](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_03/pipeline/pipeline.yml?ref_type=heads) который в себе собиратет весь оосновной функционал
3. Добавленна переменная окружения TARGET_ENV, которая имеет уникальное значение для каждого из окружений. Для примера [build_stage.yml](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_03/pipeline/build_stage.yml?ref_type=heads)

4. Добавлено несколько переменных окружения, значения которых подтягиваются из GitLab
    - [GITLAB_BUILD_MESSAGE](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_03/pipeline/build_stage.yml?ref_type=heads)
    - [GITLAB_TESTING_MESSAGE](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_03/pipeline/test_stage.yml?ref_type=heads)
    - [GITLAB_DEPLOY_MESSAGE](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_03/pipeline/deploy_stage.yml?ref_type=heads)

5. Задание со звездочкой не выполнялось.
