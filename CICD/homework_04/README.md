# Домашнее задание № 4
1. Сделать отдельный репозиторий с шаблонами CI и подключить его к своему основному репозиторию через include.

# Проделано

1. Создан отдельный [репозиторий](https://gitlab.com/mrTwice/pipeline/-/tree/main)
2. В его корне созданы несколько yml файлов, содержащих конфиг для пайплайна:

    - [build_stage.yml](https://gitlab.com/mrTwice/pipeline/-/blob/main/build_stage.yml)
    - [test_stage.yml](https://gitlab.com/mrTwice/pipeline/-/blob/main/test_stage.yml)
    - [deploy_stage.yml](https://gitlab.com/mrTwice/pipeline/-/blob/main/deploy_stage.yml)
    - [stop_stage.yml](https://gitlab.com/mrTwice/pipeline/-/blob/main/stop_stage.yml)

3. В файл [pipeline.yml](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_04/pipeline/pipeline.yml?ref_type=heads) добавленны ссылки на внешние конфиги

        include:
          - remote: https://gitlab.com/mrTwice/pipeline/-/raw/main/build_stage.yml
          - remote: https://gitlab.com/mrTwice/pipeline/-/raw/main/test_stage.yml
          - remote: https://gitlab.com/mrTwice/pipeline/-/raw/main/deploy_stage.yml
          - remote: https://gitlab.com/mrTwice/pipeline/-/raw/main/stop_stage.yml
