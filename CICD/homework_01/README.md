

# Домашнее задание № 1

* Настройка pipeline [.gitlab-ci.yml](https://gitlab.com/mrTwice/homeworks/-/blob/main/.gitlab-ci.yml?ref_type=heads)

* Макет странички [index.html](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_01/web/index.html?ref_type=heads)

* Dockerfile для [gitlab_runner](https://gitlab.com/mrTwice/homeworks/-/blob/main/CICD/homework_01/gitlab_runner/docker-compose.yml?ref_type=heads)

  * для запуска раннера в контейнере докера заполнить sample.env, и после переименовать в .env

* Ссылка на итоговую [страничку](http://mrtwice.gitlab.io/homeworks/)
